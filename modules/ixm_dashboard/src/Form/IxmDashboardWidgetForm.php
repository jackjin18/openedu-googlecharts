<?php

namespace Drupal\ixm_dashboard\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\ixm_dashboard\Utility\DisplayHelper;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides a wrapping form for editing Dashboard Display settings.
 */
class IxmDashboardWidgetForm extends ConfigFormBase {

  /**
   * The Dashboard Display Helper.
   *
   * @var \Drupal\ixm_dashboard\Utility\DisplayHelper
   */
  protected $displayHelper;

  /**
   * Constructs a new IxmDashboardPluginForm.
   *
   * @param \Drupal\ixm_dashboard\Utility\DisplayHelper $displayHelper
   *   The Dashboard Display Manager Helper.
   */
  public function __construct(DisplayHelper $displayHelper, ConfigFactoryInterface $config_factory) {
    parent::__construct($config_factory);
    $this->displayHelper = $displayHelper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('ixm_dashboard.display_helper'),
      $container->get('config.factory')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'ixm_dashboard_widgets_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [
      'ixm_dashboard.settings',
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {

    $form['header'] = [
      '#markup' => '<h3>' . $this->t('Include Widgets') . '</h3>',
      '#allowed_tags' => ['h3'],
    ];

    /** @var \Drupal\ixm_dashboard\DisplayInterface $display */
    $enabled = $this->displayHelper->getDisplaysByStatus();

    $options = [];
    $defaults = [];
    foreach ($enabled as $id => $display) {
      $options[$id] = $display->label();
      if ($display->showWidget()) {
        $defaults[] = $id;
      }
    }

    // No displays.
    if (empty($options)) {
      $form['description'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['description'],
        ],
        'markup' => [
          '#markup' => $this->t('You don\'t have any displays enabled.'),
        ],
      ];
    }
    else {
      $form['description'] = [
        '#type' => 'container',
        '#attributes' => [
          'class' => ['description'],
        ],
        'markup' => [
          '#markup' => $this->t('Select which display widgets you\'d like enabled on this page.'),
        ]
      ];

      $form['widgets'] = [
        '#type' => 'checkboxes',
        '#options' => $options,
        '#default_value' => $defaults,
      ];


      $form['actions'] = ['#type' => 'actions'];
      $form['actions']['submit'] = [
        '#type' => 'submit',
        '#value' => t('Save'),
      ];
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    parent::submitForm($form, $form_state);

    $config = $this->config('ixm_dashboard.settings');
    $widgets = $form_state->getValue('widgets');

    foreach ($widgets as $id => $enabled) {
      $enable = !empty($enabled);

      /** @var \Drupal\ixm_dashboard\DisplayInterface $display */
      $display = $this->displayHelper->getDisplays($id);
      $display->setConfigurationValue('widget', $enable);

      // Update the collection.
      $this->displayHelper->setDisplayConfig($id, $display->getConfiguration());

      // Save to base config.
      $config
        ->set('displays.' . $id, $display->getConfiguration())
        ->save();
    }

    // Back to dashboard.
    $form_state->setRedirect('ixm_dashboard.dashboard');
  }

}
